from rest_framework.decorators import api_view, permission_classes, parser_classes
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework import status

from rest_framework_simplejwt.tokens import RefreshToken

from django.contrib import auth
from django.conf import settings

import json

from .forms import LoginForm, CarCreateForm, CarEditForm, RegisterForm
from .models import Car, User

@api_view(['POST'])
def login(request):

    decode_request_body = json.loads(request.body.decode('utf-8'))

    login_form = LoginForm(decode_request_body)

    if login_form.is_valid():
        email, password = login_form.cleaned_data['email'], login_form.cleaned_data['password']

        user = auth.authenticate(email=email, password=password)

        if user is not None and user.is_active:
            auth.login(request, user)
        
            refresh = RefreshToken.for_user(user)

            return Response({
                'id': user.id,
                'token': {
                    'refresh': str(refresh),
                    'access': str(refresh.access_token),
                }
            })
        else:
            return Response(
                {
                    'message': 'Invalid values'
                },
                status.HTTP_404_NOT_FOUND
            )
    else:
        return Response(
            {
                'message': 'Invalid values'
            },
            status.HTTP_404_NOT_FOUND
        )

@api_view(['POST'])
def register(request):

    decode_request_body = json.loads(request.body.decode('utf-8'))

    register_form = RegisterForm(decode_request_body)

    if register_form.is_valid():
        username, email, password = register_form.cleaned_data['username'], register_form.cleaned_data['email'], register_form.cleaned_data['password']

        User.objects.create_user(username=username, email=email, password=password)

        user = auth.authenticate(email=email, password=password)

        if user is not None and user.is_active:
            auth.login(request, user)
        
            refresh = RefreshToken.for_user(user)

            return Response({
                'id': user.id,
                'token': {
                    'refresh': str(refresh),
                    'access': str(refresh.access_token),
                }
            })
        else:
            return Response(
                {
                    'message': 'Invalid values'
                },
                status.HTTP_404_NOT_FOUND
            )
    else:
        return Response(
            {
                'message': 'Invalid values'
            },
            status.HTTP_404_NOT_FOUND
        )

@api_view(['POST'])
@permission_classes([IsAuthenticated])
@parser_classes([MultiPartParser])
def car_create(request):

    car_create_form = CarCreateForm(request.POST)

    if car_create_form.is_valid():

        images = request.FILES.getlist('images')
        
        user = User.objects.get(id = car_create_form.cleaned_data['id'])

        car = Car.objects.create(
            id_user=user,
            name=car_create_form.cleaned_data['name'],
            model=car_create_form.cleaned_data['model'],
            color=car_create_form.cleaned_data['color'], 
            year=car_create_form.cleaned_data['year'],
            value=car_create_form.cleaned_data['value']
        )

        car.createDefaultFolders()
        
        for i, image in enumerate(images):
            car.saveImageFile(str(i + 1), image)

        return Response({
            'id': car.id_car,
            'url': f'{str(settings.URL_SITE)}{car.path_image}{car.id_car}',
            'quantity_files': len(images)
        })
    else:
        return Response(
            {
                'message': 'Invalid values'
            },
            status.HTTP_404_NOT_FOUND
        )

@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def car_edit(request, id):
    
    car_edit_form = CarEditForm(json.loads(request.body.decode('utf-8')))

    if car_edit_form.is_valid():

        car = Car.objects.get(id_car = id)        
        car.update(car_edit_form.cleaned_data)

        return Response({
                'message': f'Car updated'
            })
    else:
        return Response(
            {
                'message': 'Invalid values'
            },
            status.HTTP_404_NOT_FOUND
        )

@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def car_delete(request, id):

    car = Car.objects.get(id_car = id)        
    
    car.delete()

    return Response({
        'message': f'Car deleted'
    })


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def car_list_token(request, id):

    cars_filtered = Car.objects.filter(id_user = id)

    cars = []

    for car in cars_filtered:
        cars.append(car.dictToJSON())

    return Response({
        'cars': cars
        }
    )

@api_view(['GET'])
def car_list(request):

    cars_filtered = Car.objects.all()

    cars = []

    for car in cars_filtered:
        cars.append(car.dictToJSON())

    return Response({
        'cars': cars
        }
    )

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def car_search_token(request):

    search = request.GET['search']

    cars_filtered = Car.objects.filter(name__icontains=search)

    cars = []

    for car in cars_filtered:
        cars.append(car.dictToJSON())

    return Response({
        'cars': cars
        }
    )

@api_view(['GET'])
def car_search(request):

    search = request.GET['search']

    cars_filtered = Car.objects.filter(name__icontains=search)

    cars = []

    for car in cars_filtered:
        cars.append(car.dictToJSON())

    return Response({
        'cars': cars
        }
    )

