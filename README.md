# Web Site

### Proposta da solução

- Desenvolver um sistema com um catálogo de veículos a venda.

## UI Design

Para preparação do projeto, foi desenvolvido um protótipo de um design da possível interface que foi desenvolvida no frontend, para desenvolver esse design, foi utilizado a ferramenta [Figma](https://www.figma.com/).

O protótipo do design pode ser visualizado [clicando aqui](https://www.figma.com/proto/Tm3q8WKjkGnJYKNYkV29qk/Verzel?node-id=1%3A3&scaling=min-zoom&page-id=0%3A1&starting-point-node-id=1%3A3)

## Instalação

O projeto é dividido em duas partes, o __frontend__ feito com framework React.JS e o __backend__ uma API REST desenvolvida com o framework Django.

### Frontend

Após baixar o projeto ou fazer o clone do repositório, certifique-se que possui o Node.JS instalado em seu computador para a instalação das dependências do projeto, caso não possua, [clique aqui](https://nodejs.org/en/). Entre na pasta frontend, abra um terminal neste diretório e insira esse comando:

```bash
    npm i
```
Ao instalar as dependências do projeto, no mesmo diretório, digite __npm start__, para iniciar o servidor local do frontend.

### Backend

Certifique-se que o Python está instalado em seu computador, caso não possua [clique aqui](https://www.python.org/downloads/release/python-370/). Acesse a pasta backend do projeto, abra um terminal neste mesmo diretório e insira o commando abaixo:

```bash
   pip install -r requirements.txt
```

Caso o último comando não funcione, devido ao comando pip não achar a versão especifica das bibliotecas, sugiro que abra o arquivo requirements.txt para ver quais bibliotecas precisam ser instaladas, e instalar cada uma individualmente.

Após a instalação das dependências, insira o seguinte comando para iniciar o servidor local do backend:

```bash
   python manage.py runserver
```
## Funcionalidades

- Vitrine para a visualização dos carros com filtros de busca, carros com maior valor e menor valor, carros mais novos e mais antigos.
- Login
- Registrar novo usuário
- Visualizar carros cadastrados
- Cadastrar carros com images
- Editar carro cadastrado
- Excluir carro cadastrado


## Documentação da API

### Obter token de autenticação JWT

#### Ao enviar uma requisição, o servidor retorna o token JWT.

```http
  POST token/
```

| Parâmetro   | Tipo       | 
| :---------- | :--------- | 
| `email` | `string` | 
| `password` | `string` |  

### Login do usuário

#### Ao enviar uma requisição, o servidor retorna o id do usuário e o token JWT.

```http
  POST login/
```

| Parâmetro   | Tipo       | 
| :---------- | :--------- | 
| `email` | `string` | 
| `password` | `string` |  

### Registrar um usuário

#### Ao enviar uma requisição, o servidor retorna o id do usuário e o token JWT.

```http
  POST register/
```

| Parâmetro   | Tipo       | 
| :---------- | :--------- | 
| `nome` | `string` | 
| `email` | `string` | 
| `password` | `string` | 

### Criar um novo carro

#### Ao enviar uma requisição com as informações válidas, o servidor retorna o id do carro, URL para as imagens do carro e a quantidade de imagens.

```http
  POST car/create/
```

| Parâmetro   | Tipo       | Descrição                           |
| :---------- | :--------- | :---------------------------------- |
| `id_user` | `number` | **Obrigatório**. A chave da sua API |
| `name` | `string` | **Obrigatório**. A chave da sua API |
| `model` | `string` | **Obrigatório**. A chave da sua API |
| `color` | `string` | **Obrigatório**. A chave da sua API |
| `year` | `number` | **Obrigatório**. A chave da sua API |
| `value` | `number` | **Obrigatório**. A chave da sua API |

### Obter lista de carros registrados com token JWT

#### Ao enviar uma requisição, o servidor retorna uma lista de carros baseado no id do usuário enviado.

```http
  GET car/list_token/<id>
```

| Parâmetro   | Tipo       | Descrição                           |
| :---------- | :--------- | :---------------------------------- |
| `id_user` | `number` | **Obrigatório**. A chave da sua API |

### Obter lista de todos os carros cadastrados no servidor

#### Ao enviar uma requisição, o servidor retorna uma lista de todos os carros cadastrados.

```http
  GET car/list/
```

### Editar informações de um carro

#### Ao enviar uma requisição, o servidor retorna uma mensagem com resposta positiva da edição do carro.

```http
  GET car/edit/<id>
```

| Parâmetro   | Tipo       | Descrição                           |
| :---------- | :--------- | :---------------------------------- |
| `id_car` | `number` | **Obrigatório**. A chave da sua API |
| `name` | `string` | **Obrigatório**. A chave da sua API |
| `model` | `string` | **Obrigatório**. A chave da sua API |
| `color` | `string` | **Obrigatório**. A chave da sua API |
| `year` | `number` | **Obrigatório**. A chave da sua API |
| `value` | `number` | **Obrigatório**. A chave da sua API |

### Apagar um carro

#### Ao enviar uma requisição, o servidor retorna uma resposta positiva da exclusão do carro.

```http
  GET car/delete/<id>
```

| Parâmetro   | Tipo       | Descrição                           |
| :---------- | :--------- | :---------------------------------- |
| `id_car` | `number` | **Obrigatório**. A chave da sua API |

### Procurar por carro com token JWT

#### Ao enviar uma requisição, o servidor retorna uma lista de carros baseado na descrição do carro enviada.

```http
  GET car/search_token/
```

| Parâmetro   | Tipo       | Descrição                           |
| :---------- | :--------- | :---------------------------------- |
| `search` | `string` | **Obrigatório**. A chave da sua API |

### Procurar por carro sem token JWT

#### Ao enviar uma requisição, o servidor retorna uma lista de carros baseado na descrição do carro enviada.

```http
  GET car/search_token/
```

| Parâmetro   | Tipo       | Descrição                           |
| :---------- | :--------- | :---------------------------------- |
| `search` | `string` | **Obrigatório**. A chave da sua API |





## Documentação de cores

| Cor               | Hexadecimal                                                |
| ----------------- | ---------------------------------------------------------------- |
| background       | ![#F4F4F4](https://via.placeholder.com/10/F4F4F4?text=+) #F4F4F4 |
| blue       | ![#031D3D](https://via.placeholder.com/10/031D3D?text=+) #031D3D |
| blue light       | ![#00A4B2](https://via.placeholder.com/10/00A4B2?text=+) #00A4B2 |
| green       | ![#6AB026](https://via.placeholder.com/10/6AB026?text=+) #6AB026 |
| green light      | ![#ABC616](https://via.placeholder.com/10/ABC616?text=+) #ABC616 |
| black       | ![#434343](https://via.placeholder.com/10/434343?text=+) #434343 |
| green light      | ![#949494](https://via.placeholder.com/10/949494?text=+) #949494 |
| green lighter      | ![#DDDDDD](https://via.placeholder.com/10/DDDDDD?text=+) #DDDDDD |


## Autores

- [@Leonardo-Neves](https://github.com/Leonardo-Neves)

