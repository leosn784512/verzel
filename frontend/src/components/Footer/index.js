import React from 'react';
import { Link } from 'react-router-dom';

import './styles.css';

// Icons
import logo_verzel_icon from '../../assets/icons/logo-verzel.svg'
import account_blue from '../../assets/icons/account-blue.svg'

export default function Footer() {
    return (
        <footer className='footer'>
            <div className='alignment wrap-footer-content'>
                <img src={logo_verzel_icon} className='header-logo-verzel' alt="logo-verzel" />
                <div className='wrap-footer-link'>
                    <p className='footer-title'>Menu</p>
                    <Link className='footer-link'>Carros Usados</Link>
                    <Link className='footer-link'>Contato</Link>
                    <Link className='footer-link'>Sobre nós</Link>
                </div>

                <div className='wrap-footer-link'>
                    <p className='footer-title'>Redes Sociais</p>
                    <Link className='footer-link'>Facebook</Link>
                    <Link className='footer-link'>Instagram</Link>
                    <Link className='footer-link'>Twitter</Link>
                </div>
            
            </div>
        </footer>
    );
}