import React from 'react';

import './styles.css';

// Image
import car_image from '../../assets/images/car.png'

export default function CardCard(props) {
    return (
        <div className='card' { ...props }>
            <img src={ props.car.images[0] } className='card-image' alt="" />
            <p className='card-title'>{ props.car.name }</p>
            <p className='card-description'>{ props.car.year } - { props.car.model }</p>
            <p className='card-value'>R$ { props.car.value }</p>
        </div>
    );
}