from django.urls import path

from . import views

app_name = 'home'

urlpatterns = [
    path('login/', views.login, name="login"),
    path('register/', views.register, name="register"),
    path('car/create/', views.car_create, name="car_create"),
    path('car/list_token/<id>', views.car_list_token, name="car_list_token"),
    path('car/list/', views.car_list, name="car_list"),
    path('car/edit/<id>', views.car_edit, name="car_edit"),
    path('car/delete/<id>', views.car_delete, name="car_delete"),
    path('car/search_token', views.car_search_token, name="car_search_token"),
    path('car/search', views.car_search, name="car_search"),
]
