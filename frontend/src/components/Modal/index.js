import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom';

import './styles.css';

// Icons
import close_blue from '../../assets/icons/close-blue.svg'
import drag_gray from '../../assets/icons/drag-gray.svg'
import delete_black from '../../assets/icons/delete-black.svg'

// Model
import Car from '../../models/car'
import User from '../../models/user'

export default function Modal(props) {

    const navigate = useNavigate()

    const { register, handleSubmit, reset, setValue, formState: { errors }} = useForm();

    const hideModalEvent = (event) => {

        let modal = document.getElementById('modal')
        let button_close = document.getElementById('button-close')

        if(event.target === modal || event.target === button_close) {
            modal.style.display = 'none'
            document.body.style.overflowY = 'visible'
            clearInputFile()
            clearDragDrop()
            reset()
            props.setContentCarModal()
            props.setSearchCarModal()
        }
    }

    const hideModal = (event) => {
        let modal = document.getElementById('modal')
        let button_close = document.getElementById('button-close')

        modal.style.display = 'none'
        document.body.style.overflowY = 'visible'
        clearInputFile()
        clearDragDrop()
        reset()
        props.setContentCarModal()
        props.setSearchCarModal()
    }

    const clearDragDrop = () => {
        let modal_files = document.getElementById('modal-files')

        let modal_drop_files_icon = document.getElementById('modal-drop-files-icon')
        let modal_drop_files_label = document.getElementById('modal-drop-files-label')

        modal_files.innerHTML = ''

        modal_files.style.display = 'none'
        modal_drop_files_icon.style.display = 'flex'
        modal_drop_files_label.style.display = 'flex'
    }

    const dropHandler = (event) => {
        event.preventDefault();
        let modal_input_file = document.getElementById('modal-input-file')
        let modal_files = document.getElementById('modal-files')

        let modal_drop_files_icon = document.getElementById('modal-drop-files-icon')
        let modal_drop_files_label = document.getElementById('modal-drop-files-label')

        modal_files.style.display = 'grid'
        modal_drop_files_icon.style.display = 'none'
        modal_drop_files_label.style.display = 'none' 

        modal_files.innerHTML = ''

        modal_input_file.files = event.dataTransfer.files

        let files = modal_input_file.files

        for(let i = 0; i < files.length; i ++) {
            let image_element = document.createElement('img')

            image_element.src = URL.createObjectURL(files[i])

            image_element.classList.add('modal-files-item')

            modal_files.appendChild(image_element)
        }
        
    }

    const dragOverHandler = (event) => event.preventDefault();

    const clickDragDropModal = (event) => {
        let modal_input_file = document.getElementById('modal-input-file')
        modal_input_file.click()
    }

    const clearInputFile = () => {
        let modal_input_file = document.getElementById('modal-input-file')
        if(modal_input_file) modal_input_file.value = ''
    }

    const changeDragDropModal = (event) => {
        let modal_input_file = document.getElementById('modal-input-file')
        let modal_files = document.getElementById('modal-files')
        
        let modal_drop_files_icon = document.getElementById('modal-drop-files-icon')
        let modal_drop_files_label = document.getElementById('modal-drop-files-label')

        modal_files.style.display = 'grid'
        modal_drop_files_icon.style.display = 'none'
        modal_drop_files_label.style.display = 'none'

        modal_files.innerHTML = ''

        let files = modal_input_file.files

        for(let i = 0; i < files.length; i ++) {
            let image_element = document.createElement('img')

            image_element.src = URL.createObjectURL(files[i])

            image_element.classList.add('modal-files-item')

            modal_files.appendChild(image_element)
        }
    }

    const showMessage = (message, bollean = false) => {
        let wrap_message_element = document.getElementById('message')

        let message_element = wrap_message_element.children[0]
        
        if(bollean) {
            wrap_message_element.style.display = 'flex'

            message_element.textContent = message
            setTimeout(() => wrap_message_element.style.display = 'none', 4000)
        } else {
            wrap_message_element.style.display = 'none'
        }
    }

    const createCar = async (data) => {
        
        let modal_input_file = document.getElementById('modal-input-file')

        try {
            // Preparing body of request
            let formData = new FormData()

            for(let i = 0; i < modal_input_file.files.length; i++) {
                formData.append("images", modal_input_file.files.item(i), modal_input_file.files.item(i).name)
            }

            let data_keys = Object.keys(data)

            for(let i = 0; i < data_keys.length; i++) {
                formData.append(data_keys[i], data[data_keys[i]])
            }

            formData.append('id', User.getUserInformation())

            // Sending request
            let response = await Car.create(formData, navigate)

            if(response.status === 200) {
                showMessage('Carro adicionado!', true)
                window.location.reload()
            }

            clearDragDrop()
            clearInputFile()
            reset()
        } catch (error) {
            console.error('[Function - create_car]', error.message)
            clearDragDrop()
            clearInputFile()
            reset()
        }
    }    

    const updateCar = async (data) => {

        try {
            
            data.id_car = props.contentCarModal.id_car
            // Sending request
            let response = await Car.update(data, navigate)

            if(response.status === 200) {
                showMessage('Carro atualizado!', true)
                window.location.reload()
            }
            
        } catch (error) {
            console.error('[Function - updateCar]', error.message)
            hideModal()
        }
    }

    const deleteCar = async (event) => {
        try {
            
            let response = await Car.delete(props.contentCarModal.id_car, navigate)

            if(response.status === 200) window.location.reload()
            
        } catch (error) {
            console.error('[Function - deleteCar]', error.message)
            hideModal()
        }
    }

    const fillInputsDataFromServer = (data) => {
        if(data) {

            // Filling input text
            let content_car_modal_keys = Object.keys(data)

            for(let i = 0; i < content_car_modal_keys.length; i ++) {
                setValue(content_car_modal_keys[i], data[content_car_modal_keys[i]])
            }

            // Filling input file

            let modal_input_file = document.getElementById('modal-input-file')
            let modal_files = document.getElementById('modal-files')

            let modal_drop_files_icon = document.getElementById('modal-drop-files-icon')
            let modal_drop_files_label = document.getElementById('modal-drop-files-label')

            modal_files.style.display = 'grid'
            modal_drop_files_icon.style.display = 'none'
            modal_drop_files_label.style.display = 'none'

            if(data.images) {
                for(let i = 0; i < data.images.length; i ++) {

                    if(modal_files.children.length < data.images.length) {
                        let image_element = document.createElement('img')

                        image_element.src = data.images[i]
        
                        image_element.classList.add('modal-files-item')
        
                        modal_files.appendChild(image_element)
                    }   
                }
            }

        }
    }

    useEffect(() => {
        if(props.contentCarModal) fillInputsDataFromServer(props.contentCarModal)
    }, [props.contentCarModal])

    return (
        <div className='modal-background' id='modal' onClick={(event) => hideModalEvent(event)} {...props}>
            <div className='modal'>
                <div className='wrap-modal-header'>

                    {
                        props.edit ? (
                            <p className='modal-title'>Editar</p>
                        ) : (
                            <p className='modal-title'>Adicionar</p>
                        )
                    }
                    
                    <div className='wrap-modal-buttons'>
                        <button className='modal-button margin-right' onClick={(event) => deleteCar(event)}>
                            <img src={delete_black} className='modal-button-icon' alt="close_blue" />
                        </button>
                        <button className='modal-button' onClick={(event) => hideModalEvent(event)}>
                            <img src={close_blue} className='modal-button-icon' id='button-close' alt="close_blue" />
                        </button>
                    </div>
                </div>
                {
                    props.edit ? (
                        <form onSubmit={handleSubmit((data) => updateCar(data))}>
                            <div className='modal-row'>
                                <div className='wrap-input'>
                                    <div className='wrap-input-label'>
                                        <label className='input-label'>Nome</label>
                                    </div>
                                    <input 
                                        {...register('name', {
                                            required: 'Insira o nome do veículo'
                                        })}
                                        className='input input-name'
                                        type="text" 
                                        />
                                </div>

                                <div className='wrap-input'>
                                    <div className='wrap-input-label'>
                                        <label className='input-label'>Valor</label>
                                    </div>
                                    <input 
                                        {...register('value', {
                                            required: 'Insira o valor do veículo'
                                        })}
                                        className='input input-name'
                                        type="text" 
                                        />
                                </div>
                            </div>
                    
                            <div className='wrap-message-two'>
                                <div className='wrap-message'>
                                    <p className='message'>{ errors.model?.message }</p>
                                </div>
                                <div className='wrap-message'>
                                    <p className='message'>{ errors.color?.message }</p>
                                </div>
                            
                            </div>

                            <div className='modal-row'>
                                <div className='wrap-input'>
                                    <div className='wrap-input-label'>
                                        <label className='input-label'>Modelo</label>
                                    </div>
                                    <input 
                                        {...register('model', {
                                            required: 'Insira um modelo'
                                        })}
                                        className='input input-model' 
                                        type="text" 
                                        />
                                </div>

                                <div className='wrap-input'>
                                    <div className='wrap-input-label'>
                                        <label className='input-label'>Cor</label>
                                    </div>
                                    <input {...register('color', {
                                            required: 'Insira uma Cor'
                                        }) }
                                        className='input input-model' 
                                        type="text" 
                                        />
                                </div>

                                <div className='wrap-input'>
                                    <div className='wrap-input-label'>
                                        <label className='input-label'>Ano</label>
                                    </div>
                                    <input {...register('year', {
                                            required: 'Insira um ano'
                                        }) }
                                        className='input input-model' 
                                        type="text" 
                                        />
                                </div>
                            </div>

                            <div className='wrap-message-three'>
                                <div className='wrap-message'>
                                    <p className='message'>{ errors.model?.message }</p>
                                </div>
                                <div className='wrap-message'>
                                    <p className='message'>{ errors.color?.message }</p>
                                </div>
                                <div className='wrap-message'>
                                    <p className='message'>{ errors.year?.message }</p>
                                </div>
                            </div>

                            <div className='modal-drop-files' id='modal-drop-area'>
                                <img src={drag_gray} className="modal-drop-files-icon" id='modal-drop-files-icon' alt="" />
                                <p className='modal-drop-files-label' id='modal-drop-files-label'>Arraste e solte a image aqui para adicionar uma ou várias images</p>
                                <div className='modal-files' id='modal-files'></div>
                            </div>

                            <button type='submit' className='button-finish'>
                                <p className='button-finish-label'>Finalizar</p>
                            </button>

                            <div className='wrap-message-custom' style={{display: 'none'}} id='message'>
                                <p className='message-custom'></p>
                            </div>
                        </form>
                    ) : (
                        <form onSubmit={handleSubmit((data) => createCar(data))}>
                            <div className='modal-row'>
                                <div className='wrap-input'>
                                    <div className='wrap-input-label'>
                                        <label className='input-label'>Nome</label>
                                    </div>
                                    <input 
                                        {...register('name', {
                                            required: 'Insira o nome do veículo'
                                        })}
                                        className='input input-name'
                                        type="text" 
                                        />
                                </div>

                                <div className='wrap-input'>
                                    <div className='wrap-input-label'>
                                        <label className='input-label'>Valor</label>
                                    </div>
                                    <input 
                                        {...register('value', {
                                            required: 'Insira o valor do veículo'
                                        })}
                                        className='input input-name'
                                        type="text" 
                                        />
                                </div>
                            </div>
                    
                            <div className='wrap-message-two'>
                                <div className='wrap-message'>
                                    <p className='message'>{ errors.model?.message }</p>
                                </div>
                                <div className='wrap-message'>
                                    <p className='message'>{ errors.color?.message }</p>
                                </div>
                            
                            </div>

                            <div className='modal-row'>
                                <div className='wrap-input'>
                                    <div className='wrap-input-label'>
                                        <label className='input-label'>Modelo</label>
                                    </div>
                                    <input 
                                        {...register('model', {
                                            required: 'Insira um modelo'
                                        })}
                                        className='input input-model' 
                                        type="text" 
                                        />
                                </div>

                                <div className='wrap-input'>
                                    <div className='wrap-input-label'>
                                        <label className='input-label'>Cor</label>
                                    </div>
                                    <input {...register('color', {
                                            required: 'Insira uma Cor'
                                        }) }
                                        className='input input-model' 
                                        type="text" 
                                        />
                                </div>

                                <div className='wrap-input'>
                                    <div className='wrap-input-label'>
                                        <label className='input-label'>Ano</label>
                                    </div>
                                    <input {...register('year', {
                                            required: 'Insira um ano'
                                        }) }
                                        className='input input-model' 
                                        type="text" 
                                        />
                                </div>
                            </div>

                            <div className='wrap-message-three'>
                                <div className='wrap-message'>
                                    <p className='message'>{ errors.model?.message }</p>
                                </div>
                                <div className='wrap-message'>
                                    <p className='message'>{ errors.color?.message }</p>
                                </div>
                                <div className='wrap-message'>
                                    <p className='message'>{ errors.year?.message }</p>
                                </div>
                            </div>

                            <div className='modal-drop-files' id='modal-drop-area' onDrop={(event) => dropHandler(event)} onDragOver={(event) => dragOverHandler(event)} onClick={(event) => clickDragDropModal(event)} onChange={(event) => changeDragDropModal(event)}>
                                <input 
                                    type="file" 
                                    id='modal-input-file' 
                                    multiple 
                                    hidden
                                    />
                                <img src={drag_gray} className="modal-drop-files-icon" id='modal-drop-files-icon' alt="" />
                                <p className='modal-drop-files-label' id='modal-drop-files-label'>Arraste e solte a image aqui para adicionar uma ou várias images</p>
                                <div className='modal-files' id='modal-files'></div>
                            </div>
                            <div className='wrap-message'>
                                <p className='message'>{ errors.images?.message }</p>
                            </div>

                            <button type='submit' className='button-finish'>
                                <p className='button-finish-label'>Finalizar</p>
                            </button>

                            <div className='wrap-message-custom' style={{display: 'none'}} id='message'>
                                <p className='message-custom'></p>
                            </div>
                        </form>
                    )
                }
                

            </div>
        </div>
    );
}