import React, { useEffect } from  'react';
import { useLocation, useNavigate } from 'react-router-dom';

import { Swiper, SwiperSlide } from 'swiper/react';

// Style
import './styles.css';

// Components
import Header from '../../components/Header';
import Footer from '../../components/Footer';

// Icons
import arrow_forward_black from '../../assets/icons/arrow-forward-black.svg'
import arrow_back_black from '../../assets/icons/arrow-back-black.svg'

import car from '../../assets/images/car.png'

export default function CarDetail(props) {

    const navigate = useNavigate()
    let location = useLocation();

    const next = (event) => {
        console.log(1)
        let wrap_slides = document.getElementById('wrap-slides')
        let slide = document.getElementById('slide')

        wrap_slides.scrollLeft += (slide.offsetWidth + 20)
    }

    const last = (event) => {
        console.log(1)
        let wrap_slides = document.getElementById('wrap-slides')
        let slide = document.getElementById('slide')

        wrap_slides.scrollLeft -= (slide.offsetWidth + 20)
    }

    const scroll = (event) => {
        console.log(event.target)
    }

    useEffect(() => {
        if(location.state.images.length > 4) {
            let button_last = document.getElementById('button-last')
            let button_next = document.getElementById('button-next')

            button_last.style.display = "flex"
            button_next.style.display = "flex"
        }
    }, [])

    useEffect(() => {
        if(!location.state) navigate('/')
    }, [location])

    return (
        <>
        <Header />
        <body className="container">
            <div className="alignment-detail">
                <div className='wrap-detail-content'>
                    <img src={location.state.images[0]} className='main-image-car' alt="car" />
                    <div className='wrap-car-content'>
                        <div className='wrap-name'>
                            <p className='car-name'>{ location.state.name }</p>
                            <div className='wrap-car-describe'>
                                <p className='car-describe'>Ano: { location.state.year }</p>
                                <p className='car-describe'>Modelo: { location.state.model }</p>
                                <p className='car-describe'>Cor: { location.state.color }</p>
                            </div>
                        </div>
                        
                        <div className='wrap-button-have-interrest'>
                            <div className='wrap-value'>
                                <p className='car-value-label'>Valor</p>
                                <p className='car-value'>R$ { location.state.value }</p>
                            </div>
                            <button className='button-have-interrest'>
                                <p className='button-have-interrest-label'>Tenho interesse!</p>
                            </button>
                        </div>
    
                    </div>

                    {
                        location.state.images ? (
                            <div className='wrap-button-slides'>
                                <div className='wrap-button'>
                                    <button className='button-last' id='button-last' onClick={last}>
                                        <img className='button-slide-icon' src={arrow_back_black} alt="" />
                                    </button>
                                    <button className='button-next' id='button-next' onClick={next}>
                                        <img className='button-slide-icon' src={arrow_forward_black} alt="" />
                                    </button>
                                </div>
                                
                                <div className='wrap-slides' id='wrap-slides'>
                                    {
                                        location.state.images.map(image => {

                                            let showInMainImage = (event) => {
                                                
                                            }

                                            return <img src={image} onClick={showInMainImage} className='slide' id='slide' alt="car" draggable="false"/>
                                        })
                                    } 
                                </div>
                                
                            </div>
                        ) : null
                    }

                    
                    
                    <div className='test4'>
                        
                    </div>
                </div>
            </div>
        </body>

        <Footer />
        </>
    );
}