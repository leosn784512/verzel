import React, { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';

import './styles.css';

// Icons
import logo_verzel_icon from '../../assets/icons/logo-verzel.svg'
import account_blue from '../../assets/icons/account-blue.svg'
import logout_blue from '../../assets/icons/logout-blue.svg'

// Classes
import User from '../../models/user'

export default function Header() {

    const navigate = useNavigate()

    const [logged, setLogged] = useState(false)

    const logout = (event) => {
        User.resetToken()
        User.resetUserInformation()
        navigate('/')
        window.location.reload()
    }

    useEffect(() => {
        User.getToken() ? setLogged(true) : setLogged(false)
    }, [])

    return (
        <header className='header'>
            <div className='alignment wrap-header-content'>
                <Link to='/' className='header-slogan'>
                    <img src={logo_verzel_icon} className='header-logo-verzel' alt="logo-verzel" />
                </Link>
                
                <div className='wrap-header-link'>
                    <Link className='header-link' to='/'>Carros Usados</Link>
                    <Link className='header-link'>Contato</Link>
                    <Link className='header-link'>Sobre nós</Link>
                    {
                        logged ? <Link className='header-link' to='/list'>Cadastrar Carros</Link> : <Link className='header-link' to='/list'></Link>
                    }
                    
                </div>

                {
                    logged ? (
                        <div className='wrap-header-buttons'>
                            <button className='header-logout' onClick={(event) => logout(event)}>
                                <img src={logout_blue} className='header-login-icon' alt="header-login-icon" />
                                Sair
                            </button>
                        </div>
                    ) : (
                        <div className='wrap-header-buttons'>
                            <Link className='header-login' to='/login'>
                                <img src={account_blue} className='header-login-icon' alt="header-login-icon" />
                                Entrar
                            </Link>  
                        </div>
                    )
                    
                }

                
            </div>
        </header>
    );
}