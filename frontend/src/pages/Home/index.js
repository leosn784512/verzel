import React, { useState, useEffect } from  'react';
import { useNavigate, createSearchParams } from 'react-router-dom';

// Style
import './styles.css';

// Components
import Header from '../../components/Header';
import CardCar from '../../components/CardCar';
import Footer from '../../components/Footer';

//Icons
import search_icon from '../../assets/icons/search-gray.svg'
import arrow_down_black from '../../assets/icons/arrow-down-black.svg'
import search_off_black from '../../assets/icons/search-off-black.svg'
import search_white from '../../assets/icons/search-white.svg'
import close_black from '../../assets/icons/close-black.svg'

// Model
import Car from '../../models/car'

export default function Home() {

    const navigate = useNavigate()

    const [orderModalClicked, setOrderModalClicked] = useState(false);

    const [showFilterModel, setShowFilterModel] = useState(false);
    const [showFilterYear, setShowFilterYear] = useState(false);
    const [filterUsed, setFilterUsed] = useState(false);

    const [search, setSearch] = useState([])

    const [searchText, setSearchText] = useState('')

    const showModalOrder = (event) => {
        let order_modal = document.getElementById('order-modal')
        let order_button = document.getElementById('order-button')
        
        if(orderModalClicked) {
            order_modal.style.display = 'none'

            order_modal.style.left = `${order_button.offsetLeft}px`
            order_modal.style.top = `${order_button.offsetTop + order_button.offsetHeight}px`

            setOrderModalClicked(false)
        } else {
            order_modal.style.display = 'flex'
            
            order_modal.style.left = `${order_button.offsetLeft}px`
            order_modal.style.top = `${order_button.offsetTop + order_button.offsetHeight}px`
            
            setOrderModalClicked(true)
        }
    }

    const hideModalOrder = (event) => {
        
        let order_modal = document.getElementById('order-modal')
        let order_button = document.getElementById('order-button')
        let order_selected = document.getElementById('order-selected')

        if(event.target !== order_selected && order_modal) {
            order_modal.style.display = 'none'
            order_modal.style.left = `${order_button.offsetLeft}px`
            order_modal.style.top = `${order_button.offsetTop + order_button.offsetHeight}px`
            setOrderModalClicked(false)
        }
    }

    let lowerPriceFielter = (event) => {
        // Front end
        let order_selected = document.getElementById('order-selected')
        let p = event.target;
        order_selected.textContent = p.textContent

        // Back end
        let prices = search.map(car => car.value)
        let prices_sorted = prices.sort()

        let filtered = prices_sorted.map(price => {
            let car
            search.forEach(carr => {
                if(carr.value === price) car = carr
            })
            return car
        })

        setSearch(filtered)
        setFilterUsed(true)
    }

    let largePriceFielter = (event) => {
        // Front end
        let order_selected = document.getElementById('order-selected')
        let p = event.target;
        order_selected.textContent = p.textContent

        // Back end
        let prices = search.map(car => car.value)
        let prices_sorted = prices.sort()

        let filtered = prices_sorted.map(price => {
            let car
            search.forEach(carr => {
                if(carr.value === price) car = carr
            })
            return car
        })

        setSearch(filtered.reverse())
        setFilterUsed(true)
    }

    let lowerYearFielter = (event) => {
        // Front end
        let order_selected = document.getElementById('order-selected')
        let p = event.target;
        order_selected.textContent = p.textContent

        // Back end
        let years = search.map(car => car.year)
        let years_sorted = years.sort()

        let filtered = years_sorted.map(year => {
            let car
            search.forEach(carr => {
                if(carr.year === year) car = carr
            })
            return car
        })

        setSearch(filtered)
        setFilterUsed(true)
    }

    let largeYearFielter = (event) => {
        // Front end
        let order_selected = document.getElementById('order-selected')
        let p = event.target;
        order_selected.textContent = p.textContent

        // Back end
        let years = search.map(car => car.year)
        let years_sorted = years.sort()

        let filtered = years_sorted.map(year => {
            let car
            search.forEach(carr => {
                if(carr.year === year) car = carr
            })
            return car
        })

        setSearch(filtered.reverse())
        setFilterUsed(true)
    }

    const yearFilter = (event) => {
        let p = event.target

        let filtered = []
        
        search.forEach(car => {
            if(car.year === Number(p.getAttribute('year'))) filtered.push(car)
        })

        if(filtered) {
            setSearch(filtered)
            setFilterUsed(true)
        } 
    }

    const modelFilter = (event) => {
        let p = event.target

        let filtered = []
        
        search.forEach(car => {
            if(car.model === p.textContent) filtered.push(car)
        })

        if(filtered) {
            setSearch(filtered)
            setFilterUsed(true)
        } 
    }

    const resetFilter = (event) => {
        Car.getCars(setSearch)
        setFilterUsed(false)
    }

    const showFilterModelItens = () => {
        let filter_content = document.getElementById('filter-content-model')
        
        if(showFilterModel) {
            filter_content.style.display = 'none'
            setShowFilterModel(false)
        } else {
            filter_content.style.display = 'flex'
            setShowFilterModel(true)
        }
    }

    const showFilterYearItens = () => {
        let filter_content = document.getElementById('filter-content-year')
        
        if(showFilterYear) {
            filter_content.style.display = 'none'
            setShowFilterYear(false)
        } else {
            filter_content.style.display = 'flex'
            setShowFilterYear(true)
        }
    }

    const searchCar = async (event) => {
        try {
            let cars = await Car.search(searchText)
            setSearch(cars)
            setSearchText('')
        } catch(error) {
            console.error('[Function - searchCar]', error.message)
        }
    }

    document.body.addEventListener('scroll', (event) => hideModalOrder(event))
    document.body.addEventListener('click', (event) => hideModalOrder(event))

    useEffect(() => {
        Car.getCars(setSearch)
    }, [])

    useEffect(() => {

        let years = search.map(car => Number(car.year))
        
        let values = []
        
        for(let i = Math.min(...years); i <= Math.max(...years); i ++) {
            values.push(i)
        }

        let filter_content_year = document.getElementById('filter-content-year')
        filter_content_year.innerHTML = ''
        values.forEach(value => {

            let p = document.createElement('p')
            p.classList.add('filter-content-item')
            p.setAttribute('year', value)
            p.textContent = String(value)

            p.addEventListener('click', (event) => yearFilter(event))

            filter_content_year.appendChild(p)
        })
        
    }, [search])

    useEffect(() => {
        let button_reset_filter = document.getElementById('button-reset-filter')

        if(filterUsed) {
            button_reset_filter.style.display = 'flex'
        } else {
            button_reset_filter.style.display = 'none'
        }
    }, [filterUsed])

    return (
        <>
        <Header />
        <body className="container">
            
            <div className="alignment-home">
                <h1 className='title'>Carros Usados</h1>

                <div className='wrap-search'>
                    <div className='wrap-filter-title'>
                        <p className='filter-text'>Filtros</p>
                        <button onClick={resetFilter} className='button-reset-filter' id='button-reset-filter'>
                            <img src={close_black} className='button-reset-filter-icon' alt="close_black" />
                        </button>
                    </div>
                    
                    <div className='wrap-input-button'>
                        <div className='wrap-input-searchh'>
                            <input onChange={(event) => setSearchText(event.target.value)} className='input-search' type="text" placeholder='Busque pela descrição do anúncio ...'/>
                            <img src={search_icon} className='input-search-icon' alt="search_icon" />
                        </div>

                        <button className='button button-margin-left' onClick={() => searchCar()}>
                            <img src={search_white} className='button-icon' alt="search_white" />
                        </button>
                    </div>
                </div>

                {
                    search.length > 0 ? (
                        <div className='wrap-search-result-order'>
                            <div></div>
                            <div className='wrap-search-result-order-content'>
                                <p className='search-result-text'>{search.length} resultados</p>
                                <p className='order' id='order-button' onClick={showModalOrder}>Ordenar: <p className='order-selected' id='order-selected'>Maior preço</p></p>
                                <div className='order-modal' id='order-modal'>
                                    <p className='order-modal-item' onClick={largePriceFielter}>Maior preço</p>
                                    <p className='order-modal-item' onClick={lowerPriceFielter}>Menor preço</p>
                                    <p className='order-modal-item' onClick={largeYearFielter}>Mais novos</p>
                                    <p className='order-modal-item' onClick={lowerYearFielter}>Mais antigos</p>
                                </div>
                            </div>
                        </div>
                    ) : (
                        <div></div>
                    )
                }

                <div className='wrap-filter-cards'>
                    <div className='wrap-filters'>
                        <div className='filter'>
                            <button className='filter-label' onClick={showFilterModelItens}>
                                <div className='wrap-filter-content'>
                                    <p className='filter-name'>Marca</p>
                                    <img src={arrow_down_black} className='filter-icon' alt="filter-icon" />
                                </div>
                            </button>
                            <div className='filter-content' id='filter-content-model'>
                                <p className='filter-content-item' onClick={modelFilter}>Fiat</p>
                                <p className='filter-content-item' onClick={modelFilter}>Volkswagen</p>
                                <p className='filter-content-item' onClick={modelFilter}>Chevrolet</p>
                            </div>
                        </div>

                        <div className='filter'>
                            <button className='filter-label' onClick={showFilterYearItens}>
                                <div className='wrap-filter-content'>
                                    <p className='filter-name'>Ano</p>
                                    <img src={arrow_down_black} className='filter-icon' alt="filter-icon" />
                                </div>
                            </button>
                            <div className='filter-content' id='filter-content-year'>
                                <p className='filter-content-item'>2011</p>
                            
                            </div>
                        </div>
                        
                    </div>

                    {
                        search.length > 0 ? (
                            <div className='wrap-cards'>
                                {
                                    search.map((car, index, array) => {

                                        return (
                                            <CardCar onClick={() => navigate('detail', { state: car})} key={index} car={car} />
                                        )      
                                    })
                                }
                            </div>
                        ) : (
                            <div className='message-no-result'>
                                <img src={search_off_black} className='message-no-result-icon' alt="message-no-result-icon" />
                                <p className='message-no-result-text'>Opsss . . . Não encontramos nenhum resultado </p>
                            </div> 
                        )
                    }

                    
                </div>
            </div>

            
        </body>
        <Footer />
        </>
    );
}