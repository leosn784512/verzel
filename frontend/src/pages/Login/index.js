import React, { useState } from  'react';
import { Link, useNavigate  } from 'react-router-dom';
import { useForm } from 'react-hook-form'

// Style
import './styles.css';

//Icons
import background from '../../assets/icons/background.svg'
import logo_verzel_icon from '../../assets/icons/logo-verzel.svg'

import arrow_back_blue from '../../assets/icons/arrow-back-blue.svg'

// Backend
import User from '../../models/user'

export default function Login() {

    const navigate = useNavigate();

    const {register, handleSubmit, reset, clearErrors, formState: { errors }} = useForm();

    const {register: registerForm, handleSubmit: handleSubmitRegister, getValues, reset: resetRegister, clearErrors : clearErrorsRegister, formState: { errors: errorsRegister }} = useForm();

    const [showRegister, setShowRegister] = useState(false);

    const showRegisterPart = () => {
        let wrap_login = document.getElementById('wrap-login')
        let wrap_register = document.getElementById('wrap-register')
        
        if(showRegister) {
            wrap_login.style.display = 'flex'
            wrap_register.style.display = 'none'
            clearErrorsRegister()
            setShowRegister(false)
        } else {
            wrap_login.style.display = 'none'
            wrap_register.style.display = 'flex'
            clearErrors()
            setShowRegister(true)
        }
    }   

    const showMessage = (message, bollean = false) => {
        let wrap_message_element = document.getElementById('message')

        let message_element = wrap_message_element.children[0]
        
        if(bollean) {
            wrap_message_element.style.display = 'flex'

            message_element.textContent = message
            setTimeout(() => wrap_message_element.style.display = 'none', 4000)
        } else {
            wrap_message_element.style.display = 'none'
        }
    }

    const login = async (data) => {
        try {
            let { email, password } = data
    
            let response = await User.login(email, password)

            if(response.status === 200) {
                User.setToken(response.data.token)
                User.setUserInformation(response.data.id)
                navigate('/list')
            }
            
        } catch(error) {
            console.error('[Function - login]', error.message)
            reset()
            showMessage('Usuário ou senha incorreto!', true)
        }
    }

    const registerr = async (data) => {

        try {
            let { name, email, password } = data
    
            let response = await User.register(name, email, password)

            console.log(response)

            if(response.status === 200) {
                User.setToken(response.data.token)
                User.setUserInformation(response.data.id)
                navigate('/list')
            }
            
        } catch(error) {
            console.error('[Function - register]', error.message)
            reset()
            showMessage('Usuário ou senha incorreto!', true)
        }
    }

    return (
        <body className='body'>
            <img src={background} className='background' alt="background" />

            <div className='login'>
                <form className='wrap-login' id='wrap-login' onSubmit={handleSubmit((data) => login(data))}>
                    <img src={logo_verzel_icon} className='logo-verzel' alt="logo-verzel" />
                    <h1 className='login-title'>Entrar</h1>
                    <div className='wrap-input'>
                        <div className='wrap-input-label'>
                            <label className='input-label'>E-mail</label>
                        </div>
                        <input {...register('email', {
                            required: 'Insira um e-mail'
                        })} className='input' type="text"/>
                    </div>
                    <div className='wrap-message'>
                        <p className='message'>{ errors.email?.message }</p>
                    </div>
                    <div className='wrap-input'>
                        <div className='wrap-input-label'>
                            <label className='input-label'>Senha</label>
                        </div>
                        <input {...register('password', {
                            required: 'Insira uma senha'
                        })} className='input' type="password"/>
                    </div>
                    <div className='wrap-message'>
                        <p className='message'>{ errors.password?.message }</p>
                    </div>

                    <Link className='login-sign-up' onClick={showRegisterPart} >Não tem uma conta ? Registre</Link>

                    <button type='submit' className='button-sign-in'>
                        <p className='button-sign-in-label'>Entrar</p>
                    </button>

                    <div className='wrap-message-custom' style={{display: 'none'}} id='message'>
                        <p className='message-custom'></p>
                    </div>
                </form>

                <form className='wrap-register' id='wrap-register' onSubmit={handleSubmitRegister((data) => registerr(data))}>
                    <img src={logo_verzel_icon} className='logo-verzel' alt="logo-verzel" />
                    <div className='wrap-title-register'>
                        <img onClick={showRegisterPart} src={arrow_back_blue} className='register-go-back-icon' alt="register-go-back-icon" />
                        <h1 className='login-title no-margin'>Registrar</h1>
                        <div className='fake-space'></div>
                    </div>
                    
                    <div className='wrap-input'>
                        <div className='wrap-input-label'>
                            <label className='input-label'>Nome</label>
                        </div>
                        <input {...registerForm('name', {
                            required: 'Insira um nome'
                        })} className='input' type="text" />
                    </div>
                    <div className='wrap-message'>
                        <p className='message'>{ errorsRegister.name?.message }</p>
                    </div>

                    <div className='wrap-input'>
                        <div className='wrap-input-label'>
                            <label className='input-label'>E-mail</label>
                        </div>
                        <input {...registerForm('email', {
                            required: 'Insira um e-mail'
                        })} className='input' type="text" />
                    </div>
                    <div className='wrap-message'>
                        <p className='message'>{ errorsRegister.email?.message }</p>
                    </div>
                    <div className='wrap-input'>
                        <div className='wrap-input-label'>
                            <label className='input-label'>Senha</label>
                        </div>
                        <input {...registerForm('password', {
                            required: 'Insira uma senha'
                        })} className='input' type="password" />
                    </div>
                    <div className='wrap-message'>
                        <p className='message'>{ errorsRegister.password?.message }</p>
                    </div>
                    <div className='wrap-input'>
                        <div className='wrap-input-label'>
                            <label className='input-label'>Confirmar Senha</label>
                        </div>
                        <input {...registerForm('passwordconfirm', {
                            required: 'Insira uma senha',
                            validate: value => { 
                                if(getValues('password') !== value) {
                                    return 'As duas senhas precisam ser iguais'
                                }
                            }
                        })} className='input' type="password" />
                    </div>
                    <div className='wrap-message'>
                        <p className='message'>{ errorsRegister.passwordconfirm?.message }</p>
                    </div>

                    <div className='margin'></div>

                    <button type='submit' className='button-sign-in'>
                        <p className='button-sign-in-label'>Registrar</p>
                    </button>
                </form>
            </div>
        </body>
    );
}