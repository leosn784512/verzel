import api from '../services/api'

import User from './user'

export default class Car {

    static async create(data, callback_navigate) {
        try {
            let user_token = User.getToken()
            let config_header = {
                "headers": {
                    "Content-Type": "multipart/form-data",
                    "Authorization": `Bearer ${user_token.access}`
                }
            };

            return await api.post('car/create/', data, config_header)
        } catch(error) {
            console.error('[Class Car - create]', error.message)
            if(error.response.status === 401) User.logout(callback_navigate)
        }
    } 

    static async update(data, callback_navigate) {
        try {
            let user_token = User.getToken()
            let config_header = {
                "headers": {
                    "Authorization": `Bearer ${user_token.access}`
                }
            };

            return await api.put(`car/edit/${data.id_car}`, data, config_header)
        } catch(error) {
            console.error('[Class Car - update]', error.message)
            if(error.response.status === 401) User.logout(callback_navigate)
        }
    } 

    static async delete(id_car, callback_navigate) {
        try {
            let user_token = User.getToken()
            let config_header = {
                "headers": {
                    "Authorization": `Bearer ${user_token.access}`
                }
            };

            return await api.delete(`car/delete/${id_car}`, config_header)
        } catch(error) {
            console.error('[Class Car - delete]', error.message)
            if(error.response.status === 401) User.logout(callback_navigate)
        }
    } 

    static async searchToken(data, callback_navigate) {
        try {
            let user_token = User.getToken()
            let config_header = {
                "headers": {
                    "Authorization": `Bearer ${user_token.access}`
                }
            };  

            let response = await api.get(`car/search_token?search=${data}`, config_header)

            if(response.status === 200) {
                return response.data.cars
            } else {
                return []
            }
            
        } catch(error) {
            console.error('[Class Car - search_token]', error.message)
            if(error.response.status === 401) User.logout(callback_navigate)
        }
    } 

    static async search(data) {
        try {

            let response = await api.get(`car/search?search=${data}`)

            if(response.status === 200) {
                return response.data.cars
            } else {
                return []
            }
            
        } catch(error) {
            console.error('[Class Car - search]', error.message)
        }
    } 

    static async getCarsToken(callback, callback_navigate) {

        try {
            let id = User.getUserInformation()
            let user_token = User.getToken()
            let config_header = {
                "headers": {
                    "Authorization": `Bearer ${user_token.access}`
                }
            };

            let response = await api.get(`car/list_token/${id}`, config_header)
            
            callback(response.data.cars)
        } catch(error) {
            console.error('[Class Car - getCars]', error.message)
            if(error.response.status === 401) User.logout(callback_navigate)
        }        
    }
    
    static async getCars(callback) {
        try {
            let response = await api.get('car/list/')
            
            callback(response.data.cars)
        } catch(error) {
            console.error('[Class Car - getCars]', error.message)
        }        
    }
}