import React from 'react';
import {
    createBrowserRouter,
    RouterProvider,
} from "react-router-dom";

import Home from  './pages/Home';
import Login from  './pages/Login';
import ListCar from './pages/ListCar'
import CarDetail from './pages/CarDetail'

const router = createBrowserRouter([
    {
        path: "/",
        element: <Home />,
    },
    {
        path: "/login",
        element: <Login />,
    },
    {
        path: "/list",
        element: <ListCar />,
    },
    {
        path: "/detail",
        element: <CarDetail />,
    },
]);

export default function Routes() {
    return (
        <React.StrictMode>
            <RouterProvider router={router} />
        </React.StrictMode>
    );
}


