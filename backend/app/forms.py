from django import forms

class LoginForm(forms.Form):
    email = forms.EmailField(label='email', required=True)
    password = forms.CharField(label='password', required=True)

class RegisterForm(forms.Form):
    username = forms.CharField(label='username', required=True)
    email = forms.EmailField(label='email', required=True)
    password = forms.CharField(label='password', required=True)

class CarCreateForm(forms.Form):
    id = forms.IntegerField(required=True)
    name = forms.CharField(required=True)
    model = forms.CharField(required=True)
    color = forms.CharField(required=True)
    year = forms.IntegerField(required=True)
    value = forms.DecimalField(required=True)

class CarEditForm(forms.Form):
    name = forms.CharField(required=True)
    model = forms.CharField(required=True)
    color = forms.CharField(required=True)
    year = forms.IntegerField(required=True)
    value = forms.DecimalField(required=True)