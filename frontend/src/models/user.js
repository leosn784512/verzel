import api from '../services/api'

export default class User {

    static async login(email, password) {
        return await api.post('login/', {
            'email': email,
            'password': password
        })
    } 

    static async register(username, email, password) {
        return await api.post('register/', {
            'username': username,
            'email': email,
            'password': password
        })
    } 

    static logout(callback_navigate) {
        User.resetToken()
        User.resetUserInformation()
        callback_navigate('/')
        window.location.reload()
    }

    static setUserInformation(data) {
        localStorage.setItem('user', JSON.stringify(data)) 
    }

    static getUserInformation() {
        if(localStorage.getItem('user')) {
            return JSON.parse(localStorage.getItem('user'))
        } else {
            return ''
        }
    }

    static resetUserInformation() {
        if(localStorage.getItem('user')) localStorage.setItem('user', '')
    }

    static setToken(data) {
        localStorage.setItem('token', JSON.stringify(data)) 
    }

    static getToken() {
        if(localStorage.getItem('token')) {
            return JSON.parse(localStorage.getItem('token'))
        } else {
            return ''
        }
    }

    static resetToken() {
        if(localStorage.getItem('token')) localStorage.setItem('token', '')
    }
}