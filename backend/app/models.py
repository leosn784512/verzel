from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.core.files.storage import FileSystemStorage
from django.conf import settings

from backend.settings import BASE_DIR

from uuid import uuid4
import os, json

class Manager(BaseUserManager):
    def create_user(self, email, username, password=None):
        try:
            if not email:
                raise ValueError('Users must have an email address.')
            if not username:
                raise ValueError('Users must have a username.')

            user = self.model(email=self.normalize_email(email), username=username)
            user.set_password(password)
            user.save(using=self._db)
            return user
        except Exception as exception:
            print(f'[Class Manager: create_user]: {exception}')

    def create_superuser(self, email, username, password):
        try:
            user = self.create_user(
                email=self.normalize_email(email),
                username=username,
                password=password
            )

            user.is_admin = True
            user.is_staff = True
            user.is_superuser = True

            user.save(using=self._db)
            return user
        except Exception as exception:
            print(f'[Class Manager: create_superuser]: {exception}')

class User(AbstractBaseUser):

    username = models.CharField(verbose_name='username', max_length=100)
    email = models.EmailField(verbose_name='email', max_length=100, unique=True)
    
    date_joined = models.DateTimeField(auto_now_add=True)
    last_login = models.DateTimeField(auto_now_add=True)
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superadmin = models.BooleanField(default=False)
    
    objects = Manager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def has_perm(self, perm, obj=None):
        try:
            return self.is_admin
        except Exception as exception:
            print(f'[Class User: has_perm]: {exception}')
        
    def has_module_perms(self, app_label):
        try:
            return True
        except Exception as exception:
            print(f'[Class User: has_module_perms]: {exception}')

    def dictToJSON(self):
        return {
            'id': self.id,
            'username': self.username,
            'email': self.email,
            'created_date': self.created_date,
        }

    def __str__(self) -> str:
        return f'id_user: {self.id}, username: {self.username}, email: {self.email}, created_at: {self.created_date}'

class Car(models.Model):

    class UUIDEncoder(json.JSONEncoder):
        def default(self, obj):
            from uuid import UUID
            if isinstance(obj, UUID):
                # if the obj is uuid, we simply return the value of uuid
                return obj.hex
            return json.JSONEncoder.default(self, obj)

    # id_car = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    id_car = models.BigAutoField(primary_key=True, unique=True)
    id_user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    model = models.CharField(max_length=255)
    color = models.CharField(max_length=255)
    year = models.IntegerField()
    value = models.DecimalField(max_digits=8, decimal_places=2)
    path_image = models.CharField(max_length=255, blank=True)
    created_at = models.DateField(auto_now_add=True)

    BASE_DIR_CAR = str(BASE_DIR).replace(f'\\', '/')

    def update(self, data):
        
        self.name = data['name']
        self.model = data['model']
        self.color = data['color']
        self.year = data['year']
        self.value = data['value']

        self.save()

    def getURLFromImages(self):
        path_image = self.BASE_DIR_CAR + '/app/media/car/images/' + str(self.id_car)
        urls = []

        if(os.listdir(path_image)):

            images_filename = os.listdir(path_image)

            for filename in images_filename:
                urls.append(f'{settings.URL_SITE}app/media/car/images/{str(self.id_car)}/{filename}')

        return urls

    def dictToJSON(self):

        images = self.getURLFromImages()

        return {
            'id_car': self.id_car,
            'id_user': self.id_user.dictToJSON(),
            'name': self.name,
            'model': self.model,
            'color': self.color,
            'year': self.year,
            'value': self.value,
            'images': images,
            'created_at': self.created_at
        }

    def savePathCarImages(self, path):
        self.path_image = path
        self.save()

    def createDefaultFolders(self):
        path_image = self.BASE_DIR_CAR + '/app/media/car/images/' + str(self.id_car)
        self.savePathCarImages('app/media/car/images/' + str(self.id_car))
        os.mkdir(path_image)
        os.chmod(path_image, 0o777)

    def saveImageFile(self, file_name, image_file):
        try:
            path_image = self.BASE_DIR_CAR + '/app/media/car/images/' + str(self.id_car)
            fs = FileSystemStorage()
            fs.save(f'{path_image}/{file_name}.png', image_file)
        except Exception as exception:
            print(f'[Class Inference: saveImagesFile]: {exception}') 

    def __str__(self) -> str:
        return f'id_car: {self.id_car}, id_user: {self.id_user.id}, name: {self.name}, model: {self.model}, color: {self.color}, year: {self.year}, value: {self.value}, path_image: {self.path_image}, created_at: {self.created_at}'