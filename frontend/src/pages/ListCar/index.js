import React, { useState, useEffect } from  'react';
// import { useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom';

// Style
import './styles.css';

// Components
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Modal from '../../components/Modal';

// Icons
import add_white from '../../assets/icons/add-white.svg'
import search_not_found_black from '../../assets/icons/search-not-found-black.svg'
import search_gray from '../../assets/icons/search-gray.svg'
import search_white from '../../assets/icons/search-white.svg'
import refresh_black from '../../assets/icons/refresh-black.svg'

// Models
import Car from '../../models/car'

export default function ListCar() {

    const navigate = useNavigate()

    const [contentModal, setContentModal] = useState({})
    const [search, setSearch] = useState([])

    const [edit, setEdit] = useState(false)
    const [searching, setSearching] = useState(false)

    const [searchText, setSearchText] = useState('')

    const showModal = () => {
        let modal = document.getElementById('modal')

        modal.style.display = 'flex'
        
        document.body.style.overflowY = 'hidden'
        window.scrollTo(0,0)
    } 

    const searchCar = async (event) => {
        try {
            let cars = await Car.searchToken(searchText, navigate)
            setSearch(cars)
            setSearchText('')
        } catch(error) {
            console.error('[Function - searchCar]', error.message)
        }
    }

    const buttonRefresh = (event) => {
        Car.getCarsToken(setSearch, navigate)
    } 

    useEffect(() => {    
        Car.getCarsToken(setSearch, navigate)
    }, [])

    return (
        <>
            <Modal id='modal' edit={edit} setSearchCarModal={setEdit} contentCarModal={contentModal} setContentCarModal={setContentModal}/>
            <Header />
            <body className='container' id='body'>
                
                <div className="alignment-home">
                    <div className='wrap-buttons'>
                        <h1 className='title'>Carros Cadastrados</h1>
                        <div className='buttons'>

                            <button className='button-refresh' onClick={(event) => buttonRefresh(event)}>
                                <img src={refresh_black} className='button-refresh-icon' alt="refresh_black" />
                            </button>
                            
                            <div className='wrap-input-search'>
                                <input onChange={(event) => setSearchText(event.target.value)} className='input-search' id='input-search' type="text" placeholder='Busque pelo nome do produto . . .'/>
                                <img src={search_gray} className='input-search-icon' alt="search_gray" />
                            </div>

                            <button className='button button-margin-right' onClick={() => searchCar()}>
                                <img src={search_white} className='button-icon' alt="search_white" />
                            </button>

                            <button className='button' onClick={() => showModal()}>
                                <img src={add_white} className='button-icon' alt="add_white" />
                            </button>
                        </div>
                    </div>

                    {
                        search.length > 0 ? (
                            <table className='table'>
                                <tr className='table-header'>
                                    <th className='table-header-item'>ID</th>
                                    <th className='table-header-item table-header-item-name'>Nome</th>
                                    <th className='table-header-item'>Ano</th>
                                    <th className='table-header-item'>Modelo</th>
                                    <th className='table-header-item'>Cor</th>
                                    <th className='table-header-item'>Valor</th>
                                    <th className='table-header-item'>Data do Registro</th>
                                </tr>

                                {
                                    search.map((car, index, array) => {

                                        let showModalTableRow = (event) => {
                                            let table_row = event.target.parentElement
                                            let car = search[table_row.getAttribute('index')]
                                            setContentModal(car)
                                            setEdit(true)
                                            showModal()
                                        }

                                        return (
                                           
                                            <tr className='table-row' key={index} index={index} onClick={(event) => showModalTableRow(event)}>
                                                <td className='table-row-item table-row-item-first'># { car.id_car }</td>
                                                <td className='table-row-item'>{ car.name }</td>
                                                <td className='table-row-item'>{ car.year }</td>
                                                <td className='table-row-item'>{ car.model }</td>
                                                <td className='table-row-item'>{ car.color }</td>
                                                <td className='table-row-item'>R$ { String(car.value) }</td>
                                                <td className='table-row-item table-row-item-last'>{ `${String(car.created_at).split('-')[2]}/${String(car.created_at).split('-')[1]}/${String(car.created_at).split('-')[0]}` }</td>
                                            </tr> 
                                            
                                        )
                                    })
                                }
                            </table>
                        ) : (
                            <div className='wrap-messages'>
                                <img src={search_not_found_black} className='message-icon' alt="add_white" />
                                <p className='message-label'>Opps . . . Não achamos nenhum carro cadastrado!</p>   
                            </div>
                        )
                    }
                    
                    
                </div>
                
            </body>
            
            <Footer />

        </>
        
    );
}