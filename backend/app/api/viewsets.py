from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from app.api import serializers
from app import models

class CarViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, )

    serializer_class = serializers.CarSerializer
    queryset = models.Car.objects.all()
