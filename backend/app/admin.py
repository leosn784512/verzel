from django.contrib import admin

from app.models import Car, User

admin.site.register(Car)
admin.site.register(User)